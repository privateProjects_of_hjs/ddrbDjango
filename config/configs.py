# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月05日  09点59分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本
from utils.read_yaml import get_data_of_one_designated_yamlFile


# 第1步：获取目录名为config里的这个文件名为configs.yaml里的所有数据
data = get_data_of_one_designated_yamlFile(secondFloorFileName="config", resourceName="configs")

# 第2步：获取configs.yaml里的每个第一层级变量和对应的变量值
feiShu = data["feiShu"]
dingDing = data["dingDing"]
mail = data["mail"]

if __name__ == "__main__":

    print(feiShu)
    print(dingDing)
    print(mail)

    print(id(feiShu))
