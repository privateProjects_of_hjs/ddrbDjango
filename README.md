# 细节：
# -->1.在pycharm里通过新建一个名为【README.md】的md文件格式的文件就能新建成功【README.md】这个文件。
# -->2.【README.md】的知识点，可以百度这个链接：https://www.baidu.com/s?wd=README.md。
# -->3.针对这个项目的所有介绍信息都可以写在下面。

# 因为我还没学Markdown语言，所以暂时就随意写了。


# 1.生成这个项目依赖的所有库信息的命令行为：
#   pip freeze >requirements.txt  
#   注意事项：一、这条命令行要在项目根目录下执行。
#           二、执行成功这条命令行后，会在项目根目录下生成一个包含这个项目依赖的所有库信息的【requirements.txt】。

# 2.要在其他电脑(比如服务器)上安装这个项目的依赖的所有库，可以执行这条命令行：
#   pip install –r requirements.txt  
#   注意事项：一、执行成功这条命令行后，会成功安装这个项目的依赖的所有库。

# 3.为什么不在本地仓库里通过虚拟环境来创建这个django项目？
#   因为没办法实现，百度了很多资料也都说不行，所以就采取在本地仓库里直接用本地电脑真实环境来创建这个django项目。

# 4.为什么要开发工具类呢？
#   因为工具类，100%不依赖每个公司不同业务的配置数据，方便移植和维护。
#   只要工具类本身定义好了接口内部逻辑，也定义好了接口的每个入参和对应每个入参取值规则之后，我们把工具类做成服务，那么所有需要调用服务的脚本都只需要传符合条件的入参值即可！

# 5.这个项目的作用：
#   存储各种工具类，比如包含发送飞书/钉钉/企业微信/短信等信的不依赖数据配置的工具类，通过封装成多个接口服务的形式提供给下游模块使用。


# 6.发送飞书构建消息的url可以参考这个：
# http://127.0.0.1:8000/sendMessage/?origin=feiShu&webHookKey=5e7b40f5-759e-46e9-9701-09b0ac52222f&signKey=yUbEMi4sRBoaxPF6CxU6Qh&job_name=test-pet-business&git_branch=origin/feature_v2.0.0&build_number=2&build_url=https://jenkins.ddrbjt.com/job/uatpet-plat-gateway/1/&status=成功

# 7.发送钉钉构建消息的url可以参考这个：
# http://127.0.0.1:8000/sendMessage/?origin=dingDing&webHookKey=3743ecd6472012f1ae25167d49128bff78a2bfd6d1a8f501725afeedd30f4182&signKey=SEC34edaa279c4dd076c4925b3964351ec0879e53e8fba1733779e78fdd8b8ce210&job_name=test-pet-business&git_branch=origin/feature_v2.0.0&build_number=2&build_url=https://jenkins.ddrbjt.com/job/uatpet-plat-gateway/1/&status=成功

# 8.发送邮箱构建消息的url可以参考这个：
# http://127.0.0.1:8000/sendMessage/?origin=mail&job_name=test-pet-business&git_branch=origin/feature_v2.0.0&build_number=2&build_url=https://jenkins.ddrbjt.com/job/uatpet-plat-gateway/1/&status=成功&recipient=727803257@qq.com,3053749194@qq.com