from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse


# Create your views here.


# 细节：
# -->1.这个脚本名称原本为系统生成的【views.py】。
# -->2.为了方便区分每个应用里的同名的【views.py】，我把应用【message】里的【views.py】改名为【messageViews.py】。


# 这个函数只是用于简单的测试，不参与实际的业务逻辑
def test1(request):
    a = 666
    return HttpResponse("测试把一个值展示在网页上面，这个值为：%s" % a)


# 这个函数只是用于简单的测试，测试获取url里的入参和对应入参值，以及测试返回json数据类型的数据，不参与实际的业务逻辑
def test2(request):
    # a = request.GET["a"]  # 这种方法被废弃了，基本没人用。因为如果url里没传入参名为a的入参，就会直接导致请求报错，导致后续的脚本逻辑没法执行
    a = request.GET.get("a")  # 100%都使用这种方法了。因为如果即使url里没传入参名为a的入参，get方法有做处理了会默认把入参a的入参值设置为None所以不会导致请求报错，后续的脚本逻辑也能继续正常执行
    print(a)
    b = request.GET.get("b")
    data = {"code": a, "result": b}
    # 比如在浏览器上输入这个url：http://127.0.0.1:8000/sendMessage/?a=200&b=success
    return JsonResponse(data, json_dumps_params={"ensure_ascii": False})


from service.feiShu import FeiShu
from service.dingDing import DingDing
from service.mail import Mail


def sendMessage(request):

    origin = request.GET.get("origin")
    webHookKey = request.GET.get("webHookKey")
    signKey = request.GET.get("signKey")
    job_name = request.GET.get("job_name")
    git_branch = request.GET.get("git_branch")
    build_number = request.GET.get("build_number")
    build_url = request.GET.get("build_url")
    status = request.GET.get("status")
    # status = "未知"  # 先写死，因为我拿不到构建过程中的构建状态，只能等后续再优化
    recipient = request.GET.get("recipient")
    # print(signKey)
    # print(build_number)
    # print(recipient)

    if origin is None:
        data = {"code": 1001, "result": "fail", "msg": "入参集合里没包含入参origin！"}
    else:
        if origin == "feiShu":
            try:
                feiShu = FeiShu()
                feiShu.webHookKey = webHookKey
                feiShu.signKey = signKey
                feiShu.job_name = job_name
                feiShu.git_branch = git_branch
                feiShu.build_number = build_number
                feiShu.build_url = build_url
                feiShu.status = status
                value = feiShu.sendMessage()
                # print(value)
                if "code" in value:
                    data = {"code": 1004, "result": "fail", "msg": "入参signKey的签名校验值有误，请上传正确的签名校验值！"}
                else:
                    data = {"code": 1005, "result": "success",
                            "msg": "工程的最新构建消息已成功发送到指定的一个飞书群！这个飞书群的webHookKey值为：%s" % webHookKey}

            except Exception as e:
                data = {"code": 1003, "result": "fail",
                        "msg": "脚本执行过程中出错，导致获取不到飞书的接口返回值，建议查看url里的每个入参和对应入参值是否都正确，具体原因为:%s" % e}

        elif origin == "dingDing":
            try:
                dingDing = DingDing()
                dingDing.webHookKey = webHookKey
                dingDing.signKey = signKey
                dingDing.job_name = job_name
                dingDing.git_branch = git_branch
                dingDing.build_number = build_number
                dingDing.build_url = build_url
                dingDing.status = status
                value = dingDing.sendMessage()
                # print(value)
                if "errcode" in value:
                    if str(value["errcode"]) == "0":
                        data = {"code": 2006, "result": "success",
                                "msg": "工程的最新构建消息已成功发送到指定的一个钉钉群！这个钉钉群的webHookKey值为：%s" % webHookKey}
                    elif str(value["errcode"]) == "310000":
                        data = {"code": 2004, "result": "fail", "msg": "入参signKey的签名校验值有误，请上传正确的签名校验值！"}
                    else:
                        data = {"code": 2005, "result": "fail", "msg": "钉钉的接口返回值为：%s！请进行排查！" % value}
                else:
                    data = {"code": 2007, "result": "fail", "msg": "钉钉的接口返回值里没包含参数errcode，表示该接口已经被改造了，我们也要跟着改造对应脚本内容！"}
            except Exception as e:
                data = {"code": 2003, "result": "fail",
                        "msg": "脚本执行过程中出错，导致获取不到钉钉的接口返回值，建议查看url里的每个入参和对应入参值是否都正确，具体原因为:%s" % e}
        elif origin == "weChat":
            data = {"code": 3001, "result": "fail", "msg": "微信的消息推送功能暂未开发！"}
        elif origin == "mail":
            try:
                mail = Mail()
                mail.job_name = job_name
                mail.git_branch = git_branch
                mail.build_number = build_number
                mail.build_url = build_url
                mail.status = status
                mail.recipient = recipient
                mail.sendMessage()
                data = {"code": 4006, "result": "success", "msg": "工程的最新构建消息已成功发送到指定的邮箱！"}

            except Exception as e:
                data = {"code": 4003, "result": "fail",
                        "msg": "脚本执行过程中出错，导致邮箱发送失败，建议查看url里的每个入参和对应入参值是否都正确，具体原因为:%s" % e}
        else:
            data = {"code": 1002, "result": "fail", "msg": "入参origin的入参值只能为feiShu/dingDing/weChat/mail！"}

    return JsonResponse(data, json_dumps_params={"ensure_ascii": False})
