# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月20日  10点01分
@contactInformation: 727803257@qq.com
@Function: 发送邮件
"""

# 请从这行往下开始编写脚本
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class SendMail:
    """发送邮件"""

    # 发件人的邮箱
    sender = ""
    # 邮箱密码(qq邮箱的邮箱密码是授权码)
    psw = ""
    # 邮箱服务器域名
    smtpServer = ""
    # 邮箱服务器端口号
    port = ""

    def send(self, subject, message, recipient):
        """
        :param subject: str 邮件主题
        :param message: str 邮件内容(内容可以包含html内容)
        :param recipient: str 收件人(多个收件人用','隔开。比如："xx1@qq.com,xx2@163.com")
        """
        # 填写邮件内容
        msg = MIMEMultipart()
        body = MIMEText(message, _subtype='html', _charset='utf-8')
        msg.attach(body)
        # 填写邮件主题
        msg['Subject'] = subject
        # 填写发件人的邮箱
        msg["from"] = self.sender
        # 填写收件人的邮箱
        msg["to"] = recipient
        # 成功登录发件人的邮箱
        smtp = smtplib.SMTP_SSL(self.smtpServer, self.port)
        smtp.login(self.sender, self.psw)
        # 发送邮件
        smtp.sendmail(self.sender, recipient.split(','), msg.as_string())
        # 退出邮箱
        smtp.quit()
        # 打印成功发送邮件了的信息
        print('邮件已发送至指定邮箱，请注意查收!')


if __name__ == "__main__":

    from config import configs
    sendMail = SendMail()
    sendMail.sender = configs.mail["sender"]
    sendMail.psw = configs.mail["psw"]
    sendMail.smtpServer = configs.mail["smtpServer"]
    sendMail.port = configs.mail["port"]
    subjects = "打包部署情况"
    messages = '''
                <!DOCTYPE html>    
                <html>    
                
                <head>    
                <meta charset="UTF-8">      
                </head>    

                <body leftmargin="8" marginwidth="0" topmargin="8" marginheight="4"    
                    offset="0">    
                    <table width="95%" cellpadding="0" cellspacing="0"  style="font-size: 11pt; font-family: Tahoma, Arial, Helvetica, sans-serif">    
                        <tr>    
                            本邮件由系统自动发出，无需回复！<br/>   
                        </tr>    
                        <tr>    
                            <td><br />    
                            <b><font color="#0B610B">打包部署情况</font></b>    
                            <hr size="2" width="100%" align="center" /></td>    
                        </tr>    
                        <tr>    
                            <td>    
                                <ul>    
                                    <li>工程名称： $job_name</li>    
                                    <li>构建分支： $git_branch</li>    
                                    <li>构建编号： $build_number</li>    
                                    <li>健康状态： $status</li>    
                                    <li>构建地址： <a href="$build_url">$build_url</a></li>    
                                    <li>构建日志： <a href="$logAddress">$logAddress</a></li>     
                                    <li>完成时间： $completionTime</li>    
                                </ul>    
                            </td>    
                        </tr>    
                    </table>    
                </body>    
                
                </html>
                    '''
    from string import Template
    messages = Template(messages)
    messages = messages.safe_substitute(
        {"job_name": "test-pet-business",
         "git_branch": "origin/feature_v2.0.0",
         "build_number": 2,
         "status": "成功",
         "build_url": "http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/",
         "logAddress": "http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/console",
         "completionTime": "2021-08-19 13:09:27", }
    )

    recipients = "727803257@qq.com,3053749194@qq.com"
    sendMail.send(subject=subjects, message=messages, recipient=recipients)
