# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年04月13日  15点30分
@contactInformation: 727803257@qq.com
@Function: 提供常用的造底层数据的方法
"""

# 请从这行往下开始编写脚本


import time
import random
import datetime


# 注意事项：
# -->1.旧接口就尽量保留原状，我们不要去动到旧接口，因为旧接口可能被多个地方使用了，旧接口的改造或删除可以等后期有时间再专项处理；
# -->2.如果要实现新功能，按照开发流程的正确处理方式是尽量要新写一个接口，尽量不要对原先的旧接口进行改造，因为怕改造改出了一系列bug；

class Common:
    """提供常用的造底层数据的方法"""

    @classmethod
    def phoneNumber_of_one_random(cls, dataType: str):
        """
        :param  dataType 数据类型，数据类型为str
        :return  phoneNumber 一个符合规则的11位的手机号码，数据类型为str或int

        # 细节：
        # --》1.遇到不懂的地方要学会多百度，很多现成的方法都能通过百度找到，我们可以拿来二次封装，可以避免花大量工作时间用于重复造轮子
        # --》2.英文"dataType"的含义：数据类型
        # --》3.英文"phoneNumber"的含义：手机号码
        # --》4.英文"network_identification_code"的含义：网络识别码
        # --》5.英文"eight_digits"的含义：八位数
        """

        # 第1步：对每个入参的入参值进行指定的规则校验和数据处理
        if dataType != "str" and dataType != "int":
            raise Exception("入参dataType的入参值只能为'str'或'int'")

        # 第2步：从网络上找到目前中国三大电信运营商使用中的27个网络识别号
        network_identification_code = ['134', '135', '136', '137', '138', '139', '150', '151', '152',
                                       '158', '159', '157', '182', '187', '188', '147', '130', '131',
                                       '132', '155', '156', '185', '186', '133', '153', '180', '189']
        # 第3步：随机获取一个网络识别码
        network_identification_code_of_one_random = random.choice(network_identification_code)

        # 第4步：随机获取取值范围为00000000到99999999的8位数的一个值
        eight_digits_of_one_random = str(random.randint(0, 99999999)).zfill(8)

        # 第5步：拿第三步和第四步得到的数据进行拼接，得到一个11位的数据类型为str的手机号码
        phoneNumber = network_identification_code_of_one_random + eight_digits_of_one_random

        # 第6步：拿入参dataType的入参值当判断条件，来决定返回值的数据类型为int还是为str
        if dataType == "str":
            return phoneNumber
        else:
            phoneNumber = int(phoneNumber)
            return phoneNumber

    @classmethod
    def positiveInteger_of_one_random_singleDigit(cls, dataType: str):
        """
        :param  dataType 数据类型，数据类型为str
        :return  number  取值范围为1到9的一个正整数，数据类型为str或int

        # 细节：
        # --》1.英文"dataType"的含义：数据类型
        # --》2.英文"random"的含义：随机的
        # --》3.英文"singleDigit"的含义：个位数
        # --》4.英文"positiveInteger"的含义：正整数
        """

        # 第1步：对每个入参的入参值进行指定的规则校验和数据处理
        if dataType != "str" and dataType != "int":
            raise Exception("入参dataType的入参值只能为'str'或'int'")

        # 第2步：得到一个取值范围为1到9的数据类型为int的正整数
        number = random.randint(1, 9)

        # 第3步：拿入参dataType的入参值当判断条件，来决定返回值的数据类型为int还是为str
        if dataType == "str":
            return str(number)
        else:
            return number

    @classmethod
    def nowTime_of_ymd(cls, enum: int):
        """
        :param  enum 枚举值，数据类型为int
        :return  nowTime 一个当前系统时间,数据格式为"年月日"，数据类型为str
        # 细节：
        # --》1.接口返回值的数据内容目前有四种，分别为：
        # --》1.1.当入参enum的入参值为1，返回值的数据内容为："20210628"
        # --》1.2.当入参enum的入参值为2，返回值的数据内容为："2021-06-28"
        # --》1.3.当入参enum的入参值为3，返回值的数据内容为："2021_06_28"
        # --》1.4.当入参enum的入参值为4，返回值的数据内容为："2021年6月28日"
        # --》2.英文"original"的含义：原始
        """

        # 第1步：对每个入参的入参值进行指定的规则校验和数据处理
        if not isinstance(enum, int):
            raise Exception("入参enum的入参值的数据类型只能为'int'")

        if enum != 1 and enum != 2 and enum != 3 and enum != 4:
            raise Exception("入参dataType的入参值只能是取值范围为1到4里的任意一个值")

        # 第2步：获取包含年月日的当前系统时间的时间信息
        today = datetime.date.today()
        # 第2.1步：获取当前系统时间所属的年份
        year = str(today.year)
        # 第2.2步：获取当前系统时间所属的月份
        month = str(today.month)
        # 第2.3步：获取当前系统时间所属的日期
        day = str(today.day)

        # 第3.1步：对月份的值做优化
        if len(month) == 1:
            month_of_original = month
            month = "0" + month
        else:
            month_of_original = month
        # 第3.2步：对日期的值做优化
        if len(day) == 1:
            day_of_original = day
            day = "0" + day
        else:
            day_of_original = day

        # 第4步：拿入参enum的入参值当判断条件，来决定返回值的数据内容
        if enum == 1:
            nowTime = year + month + day
            return nowTime
        if enum == 2:
            nowTime = year + "-" + month + "-" + day
            return nowTime
        if enum == 3:
            nowTime = year + "_" + month + "_" + day
            return nowTime
        if enum == 4:
            nowTime = year + "年" + month_of_original + "月" + day_of_original + "日"
            return nowTime

    @classmethod
    def nowTime_of_ymdHMS(cls, enum: int):
        """
        :param  enum 枚举值，数据类型为int
        :return  nowTime 一个当前系统时间,数据格式为"年月日时分秒"，数据类型为str
        # 细节：
        # --》1.接口返回值的数据内容目前有四种，分别为：
        # --》1.1.当入参enum的入参值为1，返回值的数据内容为："20210628150932"
        # --》1.2.当入参enum的入参值为2，返回值的数据内容为："2021_06_28_15_09_32"
        # --》1.3.当入参enum的入参值为3，返回值的数据内容为："2021-06-28 15:09:32"
        # --》1.4.当入参enum的入参值为4，返回值的数据内容为："2021年6月28日15时9分32秒"
        """

        # 第1步：对每个入参的入参值进行指定的规则校验和数据处理
        if not isinstance(enum, int):
            raise Exception("入参enum的入参值的数据类型只能为'int'")

        if enum != 1 and enum != 2 and enum != 3 and enum != 4 and enum != 5:
            raise Exception("入参dataType的入参值只能是取值范围为1到5里的任意一个值")

        # 第2步：获取到当前系统时间的时间信息
        nowTime = time.localtime(time.time())
        # 第2.1步：获取当前系统时间所属的年份
        year = str(nowTime.tm_year)
        # 第2.2步：获取当前系统时间所属的月份
        month = str(nowTime.tm_mon)
        # 第2.3步：获取当前系统时间所属的日期
        day = str(nowTime.tm_mday)
        # 第2.4步：获取当前系统时间所属的小时
        hour = str(nowTime.tm_hour)
        # 第2.5步：获取当前系统时间所属的分钟
        minute = str(nowTime.tm_min)
        # 第2.6步：获取当前系统时间所属的秒
        second = str(nowTime.tm_sec)

        # 第3步：拿入参enum的入参值当判断条件，来决定返回值的数据内容
        if enum == 1:
            nowTime = time.strftime('%Y%m%d%H%M%S', nowTime)
            return nowTime
        if enum == 2:
            nowTime = time.strftime('%Y_%m_%d_%H_%M_%S', nowTime)
            return nowTime
        if enum == 3:
            nowTime = time.strftime('%Y-%m-%d %H:%M:%S', nowTime)
            return nowTime
        if enum == 4:
            nowTime = year + "年" + month + "月" + day + "日" + hour + "时" + minute + "分" + second + "秒"
            return nowTime

    @classmethod
    def absolutePath_of_picture_of_one_random(cls):
        """
        :return  返回在指定资源服务器里的指定一张图片的绝对路径，数据类型为str
        # 细节：
        # --》1.英文"absolutePath"的含义：绝对路径
        """

        # 第1步：目前只添加15个不同图片的绝对路径，后续按实际情况再进行增加
        absolutePathLists = [
            "https://ecrm.oss-cn-hangzhou.aliyuncs.com/itemPic/10000111/1589022123469.jpg",
            "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2619906488,2308366222&fm=26&gp=0.jpg",
            "https://hb3-ecrmmall.oss-cn-zhangjiakou.aliyuncs.com/upload/202004/38900b27-04d7-4a16-8b49-90b04b824b6e"
            ".jpg",
            "https://img.alicdn.com/bao/uploaded/i4/810764890/TB2jSJ1npXXXXXUXFXXXXXXXXXX_!!810764890.jpg_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i1/810764890/TB2uJnZmVXXXXaEXpXXXXXXXXXX_!!810764890.jpg_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i3/810764890/TB29j7zqpXXXXbqXpXXXXXXXXXX_!!810764890.png_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i2/O1CN01IpV5bQ1pGYmYcDPF1_!!0-fleamarket.jpg_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i4/O1CN01rwA0Ex1ZoscRiyZQM_!!0-fleamarket.jpg_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i1/O1CN01JdChOV26SKQVpxpRX_!!0-fleamarket.jpg_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i2/2250600715/O1CN011H9Vdc1eMixrHEW_!!2250600715.jpg_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i4/786061227/O1CN0109r0zi1Kw0ZKdfkd2_!!786061227.jpg_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i3/2250600715/O1CN01n35Fxs1H9Vk18dmaM_!!2250600715.png_200x200.jpg",
            "https://img.alicdn.com/bao/uploaded/i2/786061227/O1CN01cr9QQ21Kw0ZBnkhGZ_!!786061227.jpg_200x200.jpg",
            "https://img.yzcdn.cn/upload_files/2019/01/24/Fun-o3gZtCxNoTwfmV0XJIssSE09.jpg",
            "https://img.alicdn.com/bao/uploaded/i2/786061227/O1CN01cTdMei1Kw0ZDDjVLs_!!786061227.jpg_200x200.jpg",
        ]

        # 第2步：获取随机的1张图片的绝对路径
        absolutePath = random.choice(absolutePathLists)

        # 第3步：返回随机的1张图片的绝对路径
        return absolutePath


if __name__ == "__main__":
    common = Common()
    common.phoneNumber_of_one_random("int")
    common.positiveInteger_of_one_random_singleDigit("str")
    common.nowTime_of_ymd(1)
    common.nowTime_of_ymdHMS(4)
    common.absolutePath_of_picture_of_one_random()
