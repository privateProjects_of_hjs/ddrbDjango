# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年04月16日  11点41分
@contactInformation: 727803257@qq.com
@Function: 读取指定的单个yaml文件的数据
"""

# 请从这行往下开始编写脚本

import os
import sys
import yaml
from utils.getPath import GetPath

sys.path.append(os.getcwd())
getPath = GetPath()


def get_data_of_one_designated_yamlFile(secondFloorFileName="",
                                        thirdFloorFileName="",
                                        fourthFloorFileName="",
                                        fifthFloorFileName="",
                                        sixthFloorFileName="",
                                        seventhFloorFileName="",
                                        resourceName=""):
    """
    :param secondFloorFileName  第二层文件夹名，数据类型为str
    :param thirdFloorFileName   第三层文件夹名，数据类型为str
    :param fourthFloorFileName  第四层文件夹名，数据类型为str
    :param fifthFloorFileName   第五层文件夹名，数据类型为str
    :param sixthFloorFileName   第六层文件夹名，数据类型为str
    :param seventhFloorFileName 第七层文件夹名，数据类型为str
    :param resourceName         要获取的单个yaml文件名，数据类型为str
    :return 获取不是存储接口测试用例的指定的单个yaml文件的数据(对数据的数据结构不做任何改变)，数据类型为dict
    """
    yaml_path = getPath.get_absolute_path_of_one_resource(secondFloorFileName=secondFloorFileName,
                                                          thirdFloorFileName=thirdFloorFileName,
                                                          fourthFloorFileName=fourthFloorFileName,
                                                          fifthFloorFileName=fifthFloorFileName,
                                                          sixthFloorFileName=sixthFloorFileName,
                                                          seventhFloorFileName=seventhFloorFileName,
                                                          resourceName=resourceName)
    f = open(yaml_path, encoding="utf-8")
    data = yaml.load(f.read(), Loader=yaml.SafeLoader)
    return data


if __name__ == '__main__':
    get_data_of_one_designated_yamlFile(
        secondFloorFileName="app", thirdFloorFileName="operate", fourthFloorFileName="testcase",
        fifthFloorFileName="operate_memorialHall_examine_API", resourceName="query_list")
