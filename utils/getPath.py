"""
@author: hjs
@ide: PyCharm
@createTime: 2021年04月12日  15点02分
@contactInformation: 727803257@qq.com
@Function: 获取兼容所有操作系统的在项目里一个指定资源的规范的绝对路径
"""

# 请从这行往下开始编写脚本
import os
from os import path


class GetPath:
    """获取兼容所有操作系统的在项目里一个指定资源的规范的绝对路径"""

    def __init__(self):

        # 第1步：获取属于项目第二层级的文件夹名【utils】的绝对路径
        self.utilsPath = path.dirname(__file__)

        # 第2步：获取项目根文件夹名的绝对路径
        # 细节：
        # --》1.第2步的作用：不管后续项目放在硬盘里的哪个位置，我们都能获取到最新的项目根文件夹名的绝对路径，确保不会影响到脚本的正常执行
        self.rootDirectoryPath = path.dirname(self.utilsPath)

    def get_absolute_path_of_one_resource(self,
                                          secondFloorFileName="请赋值一个具体的第二级文件夹名(非必填)",
                                          thirdFloorFileName="请赋值一个具体的第三级文件夹名(非必填)",
                                          fourthFloorFileName="请赋值一个具体的第四级文件夹名(非必填)",
                                          fifthFloorFileName="请赋值一个具体的第五级文件夹名(非必填)",
                                          sixthFloorFileName="请赋值一个具体的第六级文件夹名(非必填)",
                                          seventhFloorFileName="请赋值一个具体的第七级文件夹名(非必填)",
                                          resourceName="请赋值一个具体的资源名(非必填)"
                                          ):
        """
        :param secondFloorFileName  第二层文件夹名，数据类型为str
        :param thirdFloorFileName   第三层文件夹名，数据类型为str
        :param fourthFloorFileName  第四层文件夹名，数据类型为str
        :param fifthFloorFileName   第五层文件夹名，数据类型为str
        :param sixthFloorFileName   第六层文件夹名，数据类型为str
        :param seventhFloorFileName 第七层文件夹名，数据类型为str
        :param resourceName         要获取的单个资源名，数据类型为str
        :return resourcePath        兼容所有操作系统的在项目里一个指定资源的规范的绝对路径，数据类型为str
        
        # 细节：
        # --》1.第N级文件夹的用于参考的本人定义的标准： 项目根文件夹名下的第二级文件夹就算第二级文件夹，依此类推
        # --》2.英文"floor"的含义：层
        # --》3.英文"file"的含义：文件夹
        # --》4.英文"resource"的含义：资源
        # --》5.资源的用于参考的本人定义的标准含义：一个文件夹名/图片/视频/word/pdf/txt/py脚本等等还有其他东西，即任何东西都算是一个资源
        # --》6.接口返回值例如为：'E:/pycharmProjects/petProject/config/config.py'
        
        """

        # 第1步：对每个入参的入参值进行指定的规则校验和数据处理

        if secondFloorFileName == "请赋值一个具体的第二级文件夹名(非必填)":
            secondFloorFileName = ""

        if thirdFloorFileName == "请赋值一个具体的第三级文件夹名(非必填)":
            thirdFloorFileName = ""

        if fourthFloorFileName == "请赋值一个具体的第四级文件夹名(非必填)":
            fourthFloorFileName = ""

        if fifthFloorFileName == "请赋值一个具体的第五级文件夹名(非必填)":
            fifthFloorFileName = ""

        if sixthFloorFileName == "请赋值一个具体的第六级文件夹名(非必填)":
            sixthFloorFileName = ""

        if seventhFloorFileName == "请赋值一个具体的第七级文件夹名(非必填)":
            seventhFloorFileName = ""

        if resourceName == "请赋值一个具体的资源名(非必填)":
            resourceName = ""

        # 第2步：获取只兼容windows操作系统的在项目里一个指定资源的绝对路径

        resourcePath = os.path.join(self.rootDirectoryPath, secondFloorFileName, thirdFloorFileName,
                                    fourthFloorFileName, fifthFloorFileName, sixthFloorFileName, seventhFloorFileName,
                                    resourceName)

        # 第3步：加这行代码的作用：为了兼容mac/linux/windows，得到兼容所有操作系统的在项目里一个指定资源的规范的绝对路径
        resourcePath = resourcePath.replace("\\", "/")

        # print(resourcePath)
        # 第4步：返回兼容所有操作系统的在项目里一个指定资源的规范的绝对路径
        return resourcePath


if __name__ == '__main__':
    test = GetPath()
    result11 = test.get_absolute_path_of_one_resource()
    result12 = test.get_absolute_path_of_one_resource(secondFloorFileName="utils", resourceName="log.py")
    result13 = test.get_absolute_path_of_one_resource(secondFloorFileName="controller",
                                                      thirdFloorFileName="common_captcha_API",
                                                      fourthFloorFileName="checkCaptcha",
                                                      fifthFloorFileName="caseSuite")
