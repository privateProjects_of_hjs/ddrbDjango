# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月20日  14点48分
@contactInformation: 727803257@qq.com
@Function: 提供一个底层类给发送不同渠道消息的子类使用
"""

# 请从这行往下开始编写脚本
import re
import json
import time
import hmac
import base64
import hashlib
import urllib.parse
from hashlib import sha256
from string import Template
from utils.common import Common

common = Common()


class Basic:
    # webHook地址的key，数据类型为str
    webHookKey: str
    # webHook地址的签名秘钥，数据类型为str
    signKey: str
    # 一个工程的名称，数据类型为str
    job_name: str
    # 部署的git分支，数据类型为str
    git_branch: str
    # 一个工程的第几次构建的值，数据类型为str
    build_number: str
    # 一个工程构建时生成的存储构建信息的构建地址，数据类型为str
    build_url: str
    # 一个工程的健康状态，数据类型为str
    status: str
    # 收件人(多个收件人用','隔开。比如："xx1@qq.com,xx2@163.com")
    recipient: str

    def finalBuildUrl(self):
        """
        :return: 处理过的一个构建地址，数据类型为str
        # 细节：
        # --》1.因为公司jenkins的构建地址有误，需要对构建地址做处理；
        # --》  比如存储公司jenkins的构建地址的环境变量的值为："https://jenkins.ddrbjt.com/job/uat-funeral-petsoul-front-operate-pc/92/"(这个地址访问会报错)
        # --》  我们希望得到正确的这个构建地址： "https://jenkins.ddrbjt.com/jenkins/job/uat-funeral-petsoul-front-operate-pc/92/"(这个地址可以正常访问)
        # --》2.英文"finalBuildUrl"的含义： 最终的
        """
        value = self.build_url.split("//")
        # print(value)
        value_of_pre = value[0]
        value_of_behind = value[1]
        result = re.findall('/(.+?)/job/', value_of_behind)
        # print(result)

        if len(result) == 0:
            a = value_of_behind.split("/", 1)
            # print(a)
            a_of_pre = a[0]
            # print(a_of_pre)
            a_of_behind = a[1]
            # print(a_of_behind)
            url = value_of_pre + "//" + a_of_pre + "/jenkins/" + a_of_behind
            return url
        else:
            return self.build_url

    def buildLogUrl(self):
        """
        :return:  一个工程构建时生成的存储构建日志的地址，数据类型为str
        # 细节：
        # --》1.英文"buildLogUrl"的含义： 构建日志地址
        """
        buildLogUrl = self.finalBuildUrl() + "console"
        return buildLogUrl

    @classmethod
    def completionTime(cls):
        """
        :return:  一个工程构建结束的大概时间，数据类型为str
        # 细节：
        # --》1.英文"completion"的含义： 完成/结束
        """
        completionTime = common.nowTime_of_ymdHMS(3)
        return completionTime

    def timestampAndSignOfDingDing(self):
        """
        :return:  关于钉钉的一个包含当前时间戳和sign的数据，数据类型为dict
        # 细节：
        # --》1.英文"sign"的含义：签名
        # --》2.英文"timestamp"的含义：时间戳
        # --》3.获取钉钉sign和获取飞书sign的逻辑有差别，所以才分别单独写接口逻辑
        """

        # print(self.signKey)

        secret_enc = self.signKey.encode('utf-8')
        timestamp = str(round(time.time() * 1000))
        string_to_sign = '{}\n{}'.format(timestamp, self.signKey)
        string_to_sign_enc = string_to_sign.encode('utf-8')
        hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
        sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
        value = {"timestamp": timestamp, "sign": sign}
        # print(value)
        return value

    def timestampAndSignOfFeiShu(self):
        """
        :return:  关于飞书的一个包含当前时间戳和sign的数据，数据类型为dict
        # 细节：
        # --》1.英文"sign"的含义： 签名
        # --》2.英文"timestamp"的含义：时间戳
        # --》3.获取钉钉sign和获取飞书sign的逻辑有差别，所以才分别单独写接口逻辑
        """

        # print(self.signKey)
        timestamp = str(round(time.time()))
        key = str(timestamp) + "\n" + self.signKey
        key_enc = key.encode("utf-8")
        msg = ""
        msg_enc = msg.encode("utf-8")
        hmac_code = hmac.new(key_enc, msg_enc, digestmod=sha256).digest()
        sign = base64.b64encode(hmac_code).decode('utf-8')
        value = {"timestamp": timestamp, "sign": sign}
        # print(value)
        return value

    def mailContentTemplate(self):
        """
        :return:  邮件内容，数据类型为str
        # 细节：
        # --》1.英文"mailContentTemplate"的含义： 邮箱内容模板
        """
        message = '''
                    <!DOCTYPE html>    
                    <html>    
    
                    <head>    
                    <meta charset="UTF-8">      
                    </head>    
    
                    <body leftmargin="8" marginwidth="0" topmargin="8" marginheight="4"    
                        offset="0">    
                        <table width="95%" cellpadding="0" cellspacing="0"  style="font-size: 11pt; font-family: Tahoma, Arial, Helvetica, sans-serif">    
                            <tr>    
                                本邮件由系统自动发出，无需回复！<br/>   
                            </tr>    
                            <tr>    
                                <td><br />    
                                <b><font color="#0B610B">打包部署情况</font></b>    
                                <hr size="2" width="100%" align="center" /></td>    
                            </tr>    
                            <tr>    
                                <td>    
                                    <ul>    
                                        <li>工程名称： $job_name</li>    
                                        <li>构建分支： $git_branch</li>    
                                        <li>构建编号： #$build_number</li>    
                                        <li>健康状态： $status</li>    
                                        <li>构建地址： <a href="$build_url">$build_url</a></li>    
                                        <li>构建日志： <a href="$logAddress">$logAddress</a></li>     
                                        <li>完成时间： $completionTime</li>    
                                    </ul>    
                                </td>    
                            </tr>    
                        </table>    
                    </body>    
    
                    </html>
                        '''
        message = Template(message)
        message = message.safe_substitute(
            {"job_name": self.job_name,
             "git_branch": self.git_branch,
             "build_number": self.build_number,
             "status": self.status,
             "build_url": self.finalBuildUrl(),
             "logAddress": self.buildLogUrl(),
             "completionTime": self.completionTime(), }
        )
        return message

    def feiShuContentTemplate(self):
        """
        :return:  飞书内容，数据类型为str
        # 细节：
        # --》1.英文"feiShuContentTemplate"的含义：飞书内容模板
        # --》2.英文"richText"的含义：富文本
        """
        richText = {
            "msg_type": "post",
            "content": {
                "post": {
                    "zh_cn": {
                        "title": "打包部署情况",
                        "content": [
                            [
                                {
                                    "tag": "text",
                                    "text": "工程名称: %s\n" % self.job_name
                                },
                                {
                                    "tag": "text",
                                    "text": "构建分支: %s\n" % self.git_branch
                                },
                                {
                                    "tag": "text",
                                    "text": "构建编号: #%s\n" % self.build_number
                                },
                                {
                                    "tag": "text",
                                    "text": "健康状态: %s\n" % self.status
                                },
                                {
                                    "tag": "text",
                                    "text": "构建地址: "
                                },
                                {
                                    "tag": "a",
                                    "text": "点击查看",
                                    "href": "%s" % self.finalBuildUrl()
                                },
                                {
                                    "tag": "text",
                                    "text": "\n"
                                },
                                {
                                    "tag": "text",
                                    "text": "构建日志: "
                                },
                                {
                                    "tag": "a",
                                    "text": "点击查看",
                                    "href": "%s" % self.buildLogUrl()
                                },
                                {
                                    "tag": "text",
                                    "text": "\n"
                                },
                                {
                                    "tag": "text",
                                    "text": "完成时间: %s\n" % self.completionTime()
                                },
                            ]
                        ]
                    }
                }
            }
        }

        if self.signKey is None:
            data = json.dumps(richText)
        else:
            timestampAndSign = self.timestampAndSignOfFeiShu()
            richText["timestamp"] = timestampAndSign["timestamp"]
            richText["sign"] = timestampAndSign["sign"]
            data = json.dumps(richText)
        return data

    def dingDingContentTemplate(self):
        """
        :return:  钉钉内容，数据类型为str
        # 细节：
        # --》1.英文"dingDingContentTemplate"的含义： 钉钉内容模板
        """

        data = {
            "msgtype": "markdown",
            "markdown": {
                "title": "打包部署情况",
                "text": "##### 消息标题：%s \n" % "打包部署情况" +
                        "##### 工程名称：%s \n" % self.job_name +
                        "##### 构建分支：%s \n" % self.git_branch +
                        "##### 构建编号：#%s \n" % self.build_number +
                        "##### 健康状态：%s \n" % self.status +
                        "##### 构建地址：[点击查看](%s) \n" % self.finalBuildUrl() +
                        "##### 构建日志：[点击查看](%s) \n" % self.buildLogUrl() +
                        "##### 完成时间：%s \n" % self.completionTime()
            },
            "at": {
                "atMobiles": [],
                "isAtAll": False
            }
        }

        data = json.dumps(data)
        return data

    @classmethod
    def sendMessage(cls):
        """
        :return:  发送推送消息
        # 细节：
        # --》1.这个父类方法不实现具体逻辑，具体逻辑由子类实现
        """


if __name__ == '__main__':
    basic = Basic()
    basic.webHookKey = "5e7b40f5-759e-46e9-9701-09b0ac52222f"
    basic.signKey = "yUbEMi4sRBoaxPF6CxU6Qh"
    basic.job_name = "test-pet-business"
    basic.git_branch = "origin/feature_v2.0.0"
    basic.build_number = 7
    basic.build_url = "https://jenkins.ddrbjt.com/job/uat-funeral-petsoul-front-operate-pc/92/"
    basic.status = "成功"
    basic.recipient = "727803257@qq.com,3053749194@qq.com"
    basic.finalBuildUrl()
    basic.timestampAndSignOfDingDing()
    basic.timestampAndSignOfFeiShu()
    basic.buildLogUrl()
    basic.completionTime()
    basic.mailContentTemplate()
    basic.feiShuContentTemplate()
    basic.dingDingContentTemplate()
