# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月06日  14点25分
@contactInformation: 727803257@qq.com
@Function: 对钉钉消息推送api的相关封装
"""

# 请从这行往下开始编写脚本
import requests
from config import configs
from service.basic import Basic


class DingDing(Basic):
    """对钉钉消息推送api的相关封装"""

    def sendMessage(self):
        """
        :return:  发送推送消息给指定的一个机器人所在的钉钉群并返回webHookUrl的接口返回值，数据类型为dict
        """

        timestampAndSign = self.timestampAndSignOfDingDing()
        timestamp = timestampAndSign["timestamp"]
        sign = timestampAndSign["sign"]

        webHookUrl = configs.dingDing["relativeWebHook"] + self.webHookKey + "&timestamp=" + timestamp + "&sign=" + sign
        # print(webHookUrl)
        headers = {"Content-Type": "application/json;charset=UTF-8"}

        data = self.dingDingContentTemplate()

        result = requests.post(url=webHookUrl, headers=headers, data=data)
        result = result.json()
        return result


if __name__ == '__main__':
    dingDing = DingDing()
    # 场景1-1：有设置签名的情况(build_url值为个人jenkins里的其中一个jenkins构建地址)
    dingDing.webHookKey = "3743ecd6472012f1ae25167d49128bff78a2bfd6d1a8f501725afeedd30f4182"
    dingDing.signKey = "SEC34edaa279c4dd076c4925b3964351ec0879e53e8fba1733779e78fdd8b8ce210"
    dingDing.job_name = "test-pet-business"
    dingDing.git_branch = "origin/feature_v2.0.0"
    dingDing.build_number = 7
    dingDing.build_url = "http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/"
    dingDing.status = "成功"
    dingDing.recipient = ""
    dingDing.sendMessage()

    # 场景1-2：有设置签名的情况(build_url值为公司jenkins里的其中一个jenkins构建地址)
    dingDing.build_url = "https://jenkins.ddrbjt.com/job/uatpet-plat-gateway/1/"
    dingDing.sendMessage()
