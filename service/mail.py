# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月20日  13点31分
@contactInformation: 727803257@qq.com
@Function: 对邮箱消息推送api的相关封装
"""
# 请从这行往下开始编写脚本

from config import configs
from service.basic import Basic
from utils.sendMail import SendMail


class Mail(Basic):
    """对邮箱消息推送api的相关封装"""

    def sendMessage(self):
        """
        :return:  发送推送消息给指定的邮箱
        """

        sendMail = SendMail()
        sendMail.sender = configs.mail["sender"]
        sendMail.psw = configs.mail["psw"]
        sendMail.smtpServer = configs.mail["smtpServer"]
        sendMail.port = configs.mail["port"]

        subject = "打包部署情况"
        message = self.mailContentTemplate()

        sendMail.send(subject=subject, message=message, recipient=self.recipient)


if __name__ == '__main__':
    mail = Mail()
    # 场景1-1：build_url值为个人jenkins里的其中一个jenkins构建地址
    mail.webHookKey = ""
    mail.signKey = ""
    mail.job_name = "test-pet-business"
    mail.git_branch = "origin/feature_v2.0.0"
    mail.build_number = 7
    mail.build_url = "http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/"
    mail.status = "成功"
    mail.recipient = "727803257@qq.com,3053749194@qq.com"
    mail.sendMessage()

    # 场景1-2：build_url值为公司jenkins里的其中一个jenkins构建地址
    mail.build_url = "https://jenkins.ddrbjt.com/job/uatpet-plat-gateway/1/"
    mail.sendMessage()
