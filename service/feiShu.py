# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月06日  14点25分
@contactInformation: 727803257@qq.com
@Function: 对飞书消息推送api的相关封装
"""

# 请从这行往下开始编写脚本

import requests
from config import configs
from service.basic import Basic


class FeiShu(Basic):
    """对飞书消息推送api的相关封装"""

    def sendMessage(self):
        """
        :return:  发送推送消息给指定的一个机器人所在的飞书群并返回webHookUrl的接口返回值，数据类型为dict
        """

        webHookUrl = configs.feiShu["relativeWebHook"] + self.webHookKey
        headers = {'Content-Type': 'application/json'}
        data = self.feiShuContentTemplate()

        result = requests.post(url=webHookUrl, headers=headers, data=data)
        result = result.json()
        return result


if __name__ == '__main__':
    feiShu = FeiShu()

    # 场景1-1：没设置签名的情况(build_url值为个人jenkins里的其中一个jenkins构建地址)
    feiShu.webHookKey = "5e7b40f5-759e-46e9-9701-09b0ac52222f"
    feiShu.signKey = None
    feiShu.job_name = "test-pet-business"
    feiShu.git_branch = "origin/feature_v2.0.0"
    feiShu.build_number = 7
    feiShu.build_url = "http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/"
    feiShu.status = "成功"
    feiShu.recipient = "727803257@qq.com,3053749194@qq.com"
    feiShu.sendMessage()

    # 场景1-2：没设置签名的情况(build_url值为公司jenkins里的其中一个jenkins构建地址)

    feiShu.build_url = "https://jenkins.ddrbjt.com/job/uatpet-plat-gateway/1/"
    feiShu.sendMessage()

    # 场景2-1：有设置签名的情况(build_url值为个人jenkins里的其中一个jenkins构建地址)
    feiShu.signKey = "1entzCqD48noDAS8Rgb0xe"
    feiShu.build_url = "http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/"
    feiShu.sendMessage()

    # # 场景2-2：有设置签名的情况(build_url值为公司jenkins里的其中一个jenkins构建地址)
    feiShu.signKey = "1entzCqD48noDAS8Rgb0xe11"
    feiShu.build_url = "https://jenkins.ddrbjt.com/job/uatpet-plat-gateway/1/"
    feiShu.sendMessage()

