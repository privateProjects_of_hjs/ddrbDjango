# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月19日  09点35分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本

# 细节：
# -->1.要实现一个功能，首先就是要按记流水账的形式来记录，然后等调试通过后，再按照函数或者类来对流水账里的所有脚本内容进行最终封装，方便后续维护！
# -->1.1.所以当前这个脚本里，我就是按记流水账的形式来记录并进行调试通过！
# -->1.2.这个脚本调试通过后就不要去动了，可以当成其他脚本的参考资料！
# -->1.3.因为其他脚本就会写专门的函数或者类来对这个脚本里的所有脚本内容进行最终封装，方便后续维护！


# 以下就是完整的关于邮箱推送消息的完整流程(按记流水账的形式来记录)


# 第1步：获取到一个钉钉群里的一个机器人A的唯一webHookUrl
webHookUrl = "https://oapi.dingtalk.com/robot/send?access_token=3743ecd6472012f1ae25167d49128bff78a2bfd6d1a8f501725afeedd30f4182"

# 第2步：获取到机器人A的最新的一个签名校验秘钥
# 细节：
# -->1.签名校验秘钥，用户可以更新！
# -->1.钉钉机器人要发送推送消息必须得设置至少一个安全设置，因为签名校验比较能确保消息推送的安全，所以我们选择用签名校验的这种安全设置！
signKey = "SEC34edaa279c4dd076c4925b3964351ec0879e53e8fba1733779e78fdd8b8ce210"

# 第3步：查看钉钉机器人推送消息的官方API文档
# -->1.官方API文档里有最新的关于钉钉推送消息的接口请求范例，这是对我们最有帮助的内容！
# -->2.官方API文档的数据肯定是最新的，所以没必要去看其他非官方的API文档！
# -->3.官方API文档所在的平台名是"钉钉开放平台"！
# -->4.官方API文档的地址为：https://developers.dingtalk.com/document/robots/customize-robot-security-settings

# 第4步：获取到timestamp和sign
# 细节：
# -->1.这些代码是官方提供的，所以直接拿来用就行，大概知道每行代码实现啥功能就行！
import time
import hmac
import hashlib
import base64
import urllib.parse

timestamp = str(round(time.time() * 1000))
secret = signKey
secret_enc = secret.encode('utf-8')
string_to_sign = '{}\n{}'.format(timestamp, secret)
string_to_sign_enc = string_to_sign.encode('utf-8')
hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
print(timestamp)
print(sign)

# 第5步：把timestamp和sign拼接到webHookUrl，生成一个最终的能实现钉钉推送消息功能的url
url = webHookUrl + "&timestamp=" + timestamp + "&sign=" + sign
print(url)

# 第6步：设置请求头header

header = {"Content-Type": "application/json;charset=UTF-8"}

# #################################################
# 第7.1步：设置要发送的消息模板
message_body = {
    "msgtype": "markdown",
    "markdown": {
        "title": "打包部署情况",
        "text": "##### 消息标题：%s \n" % "打包部署情况" +
                "##### 工程名称：%s \n" % "test-funeral-ysdt-operate-web" +
                "##### 构建分支：%s \n" % "origin/feature_v3.0.4"+
                "##### 构建编号：%s \n" % "#264"+
                "##### 健康状态：%s \n" % "成功"+
                "##### 构建地址：[点击查看](%s) \n"%"http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/"+
                "##### 构建日志：[点击查看](%s) \n" % "http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/console"+
                "##### 完成时间：%s \n" % "2021-08-19 13:09:27"
    },
    "at": {
        "atMobiles": [],
        "isAtAll": False
    }
}

# 第8.1步：将字典类型数据转化为json格式

import json

send_data = json.dumps(message_body)

# 第9.1步：发送接口请求
import requests

ChatBot = requests.post(url=url, data=send_data, headers=header)
opener = ChatBot.json()
if opener["errmsg"] == "ok":
    print(u"接口返回值为：%s；表示通知消息发送成功！" % opener)
else:
    print(u"通知消息发送失败，原因：{}".format(opener))

# 第10.1步：查看对应的一个钉钉群，能看到钉钉群能收到对应正确的消息内容
# #################################################


# # #################################################
# # 第7.2步：设置要发送的消息模板
# message_body = {
#     "msgtype": "feedCard",
#     "feedCard": {
#         "links": [
#             {
#                 "title": "时代的火车向前开1",
#                 "messageURL": "https://www.dingtalk.com/",
#                 "picURL": "https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png"
#             },
#             {
#                 "title": "时代的火车向前开2",
#                 "messageURL": "https://www.dingtalk.com/",
#                 "picURL": "https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png"
#             }
#         ]
#     }
# }
#
#
# # 第8.2步：将字典类型数据转化为json格式
#
# import json
# send_data = json.dumps(message_body)
#
# # 第9.2步：发送接口请求
# import requests
# ChatBot = requests.post(url=url, data=send_data, headers=header)
# opener = ChatBot.json()
# if opener["errmsg"] == "ok":
#     print(u"接口返回值为：%s；表示通知消息发送成功！" % opener)
# else:
#     print(u"通知消息发送失败，原因：{}".format(opener))
#
# # 第10.2步：查看对应的一个钉钉群，能看到钉钉群能收到对应正确的消息内容
# # #################################################


# #################################################
# 第7.3步：设置要发送的消息模板
message_body = {
    "msgtype": "markdown",
    "markdown": {
        "title": "杭州天气",
        "text": "#### 杭州天气 @150XXXXXXXX \n> 9度，西北风1级，空气良89，相对温度73%\n> ![screenshot](https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png)\n> ###### 10点20分发布 [天气](https://www.dingalk.com) \n"
    },
    "at": {
        "atMobiles": [
            "150XXXXXXXX"
        ],
        "atUserIds": [
            "user123"
        ],
        "isAtAll": False
    }
}

# 第8.3步：将字典类型数据转化为json格式

import json

send_data = json.dumps(message_body)

# 第9.3步：发送接口请求
import requests

ChatBot = requests.post(url=url, data=send_data, headers=header)
opener = ChatBot.json()
if opener["errmsg"] == "ok":
    print(u"接口返回值为：%s；表示通知消息发送成功！" % opener)
else:
    print(u"通知消息发送失败，原因：{}".format(opener))

# 第10.3步：查看对应的一个钉钉群，能看到钉钉群能收到对应正确的消息内容
#################################################
