# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月20日  13点31分
@contactInformation: 727803257@qq.com
@Function: 对邮箱消息推送api的相关封装
"""
import re
from config import configs
from utils.common import Common
from utils.sendMail import SendMail
from utils.mail.mailContentTemplate import mailContentTemplate

common = Common()


# 请从这行往下开始编写脚本
class Mail:
    """对邮箱消息推送api的相关封装"""

    @classmethod
    def __finalBuildUrl(cls, build_url):
        """
        :param build_url:     一个工程构建时生成的存储构建信息的构建地址，数据类型为str
        :return:              一个工程构建时生成的存储构建信息的构建地址，数据类型为str
        # 细节：
        # --》1.因为公司jenkins的构建地址有误，需要对构建地址做处理；
        # --》1.1.比如存储公司jenkins的构建地址的环境变量的值为："https://jenkins.ddrbjt.com/job/uat-funeral-petsoul-front-operate-pc/92/"(这个地址访问会报错)
        # --》    我们希望得到正确的这个构建地址： "https://jenkins.ddrbjt.com/jenkins/job/uat-funeral-petsoul-front-operate-pc/92/"(这个地址可以正常访问)
        """
        value = build_url.split("//")
        # print(value)
        value_of_pre = value[0]
        value_of_behind = value[1]
        result = re.findall('/(.+?)/job/', value_of_behind)
        # print(result)

        if len(result) == 0:
            a = value_of_behind.split("/", 1)
            # print(a)
            a_of_pre = a[0]
            # print(a_of_pre)
            a_of_behind = a[1]
            # print(a_of_behind)
            url = value_of_pre + "//" + a_of_pre + "/jenkins/" + a_of_behind
            return url
        else:
            return build_url

    @classmethod
    def sendRobotMessage(cls, job_name, git_branch, build_number, build_url, status, recipient):
        """
        :param job_name:       一个工程的名称，数据类型为str
        :param git_branch:     部署的git分支，数据类型为str
        :param build_number:   一个工程的第几次构建的值，数据类型为str或者为int
        :param build_url:      一个工程构建时生成的存储构建信息的构建地址，数据类型为str
        :param status:         一个工程的健康状态，数据类型为str
        :param recipient: str  收件人(多个收件人用','隔开。比如："xx1@qq.com,xx2@163.com")
        :return:               发送推送消息给指定的邮箱
        """

        build_url = cls.__finalBuildUrl(build_url)

        # 一个工程构建时生成的存储构建日志的日志地址
        logAddress = build_url + "console"

        # 一个工程构建完成的大概时间
        completionTime = common.nowTime_of_ymdHMS(3)

        sendMail = SendMail()
        sendMail.sender = configs.mail["sender"]
        sendMail.psw = configs.mail["psw"]
        sendMail.smtpServer = configs.mail["smtpServer"]
        sendMail.port = configs.mail["port"]

        subject = "打包部署情况"
        message = mailContentTemplate(job_name=job_name, git_branch=git_branch, build_number=build_number,
                                      build_url=build_url, status=status, logAddress=logAddress,
                                      completionTime=completionTime)

        sendMail.send(subject=subject, message=message, recipient=recipient)


if __name__ == '__main__':
    mail = Mail()
    # 场景1-1：有设置签名的情况(build_url值为个人jenkins里的其中一个jenkins构建地址)
    mail.sendRobotMessage(
        job_name="test-funeral-ysdt-operate-web",
        git_branch="origin/feature_v3.0.4",
        build_number="264",
        build_url="http://39.108.232.66:8080/jenkins/job/feishuApiProject/32/",
        status="成功",
        recipient="727803257@qq.com,3053749194@qq.com"
    )
