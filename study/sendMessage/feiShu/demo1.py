# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月05日  09点18分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本

import json
import requests


# 你复制的webhook地址
url = "https://open.feishu.cn/open-apis/bot/v2/hook/a2de51de-b65c-4b4c-af69-0cb4ed13aeef"
###____________________________________________发送普通text___________________________________________________
# payload_message = {
#     "msg_type": "text",
#     "content": {
#         "text": "测试一下"
#     }
# }
# headers = {
#     'Content-Type': 'application/json'
# }
#
# response = requests.request("POST", url, headers=headers, data=json.dumps(payload_message))
# print(response.text)


###_________________________________发送富文本消息_____________________________________________________
rich_text = {
    "msg_type": "post",
    "content": {
        "post": {
            "zh_cn": {
                "title": "云上殿堂项目",
                "content": [
                    [
                        {
                            "tag": "text",
                            "text": "工程test-funeral-ysdt-front-operate-pc部署完成，请稍后进行验证"
                        },
                        {
                            "tag": "a",
                            "text": "点击查看",
                            "href": "http://39.108.232.66:8080/jenkins/job/%E5%AE%A0%E7%89%A9%E6%AE%BF%E5%A0%82%E9%A1%B9%E7%9B%AE-%E6%8E%A5%E5%8F%A3%E8%87%AA%E5%8A%A8%E5%8C%96%E6%8C%81%E7%BB%AD%E9%9B%86%E6%88%90/20/"
                        }
                    ]
                ]
            }
        }
    }
}
headers = {
    'Content-Type': 'application/json'
}
response = requests.request("POST", url, headers=headers, data=json.dumps(rich_text))
print(response.text)


feiShuGroup = 1
firstRobot = 1
webHookUrl = 2
