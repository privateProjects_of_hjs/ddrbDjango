# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月09日  13点20分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

import re

# 请从这行往下开始编写脚本
# url = "https://jenkins.ddrbjt.com/jenkins/job/uat-funeral-petsoul-front-operate-pc/92/"
url = "https://jenkins.ddrbjt.com/job/uat-funeral-petsoul-front-operate-pc/92/"
value = url.split("//")
print(value)
value_of_pre = value[0]
value_of_behind = value[1]
result = re.findall('/(.+?)/job/', value_of_behind)
print(result)

if len(result) == 0:
    a = value_of_behind.split("/", 1)
    print(a)
    a_of_pre = a[0]
    print(a_of_pre)
    a_of_behind = a[1]
    print(a_of_behind)
    url = value_of_pre+"//"+a_of_pre+"/jenkins/"+a_of_behind
    print(url)

# 利用str和in的关系，来判断数据是否存在
# response = requests.get(url=logAddress)
# value = response.text
# if "Finished: FAILURE" in value:
#     status = "FAILURE"
# elif "Finished: SUCCESS" in value:
#     status = "SUCCESS"
# else:
#     status = "未知"
