# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月06日  15点48分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本
import re
import requests

url = "http://39.108.232.66:8080/jenkins/job/feishuApiProject/33/console"
headers = {'Content-Type': 'application/json'}
response = requests.get(url=url)
value=response.text
print(value)

if "Finished: FAILURE" in value:
    print(6666)

if "Finished: 2222" in value:
    print(7777)