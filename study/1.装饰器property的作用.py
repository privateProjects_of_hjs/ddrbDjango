# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年08月25日  11点48分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""
# 请从这行往下开始编写脚本

# 可参考的网站：
# --》1.https://blog.csdn.net/weixin_42681866/article/details/83376484
# --》2.https://zhuanlan.zhihu.com/p/64487092
# --》3.https://www.liaoxuefeng.com/wiki/897692888725344/923030547069856

import random


class DataSet(object):
    @property
    def method_with_property(self):  # 含有@property
        a = random.randint(1, 1000)
        print(a)
        return a

    @classmethod
    def method_without_property(cls):  # 不含@property
        return 16


r = DataSet()
print(r.method_with_property)  # 加了@property后，可以用调用属性的形式来调用方法,后面不需要加（）。
print(r.method_without_property())
print(r.method_with_property)
